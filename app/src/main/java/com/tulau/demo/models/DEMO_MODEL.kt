package com.tulau.demo.models

import java.io.Serializable

/**
 * Created by Tư Lầu ✪ on 18/06/2021 - 1:08 PM.
 */
data class DEMO_MODEL(
    var _id: String? = "",
    var title: String? = "",
    var description: String? = null
) : Serializable