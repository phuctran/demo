package com.tulau.demo.infinite_looper.views

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.tulau.demo.R
import com.tulau.demo.databinding.ActivityMainBinding
import com.tulau.demo.infinite_looper.adapters.DemoInfiniteAdapter
import com.tulau.demo.infinite_looper.viewmodels.AdsPagerViewModel


class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: AdsPagerViewModel
    private lateinit var bindingData: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bindingData = DataBindingUtil.setContentView(this, R.layout.activity_main)

        viewModel = ViewModelProvider(this).get(AdsPagerViewModel::class.java)

        bindingData.viewPagerStories.let {
            it.adapter = DemoInfiniteAdapter(viewModel?.storiesData!!, true)
            it.isInfinite = true
            it.isAutoScroll = true
            it.setTimer(3000)
            it.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                    bindingData.tvCounter.text = "${
                        if (position > (viewModel?.storiesData?.size ?: 0)) {
                            (viewModel?.storiesData?.size ?: 0)
                        } else if (position < 1) {
                            1
                        } else {
                            position
                        }
                    }/${viewModel?.storiesData?.size}"
                }

                override fun onPageSelected(position: Int) {

                }

                override fun onPageScrollStateChanged(state: Int) {

                }
            })

        }

    }

    override fun onPause() {
        super.onPause()
        bindingData.viewPagerStories?.pauseAutoScroll()
    }

    override fun onResume() {
        super.onResume()
        bindingData.viewPagerStories?.resumeAutoScroll()
    }

}