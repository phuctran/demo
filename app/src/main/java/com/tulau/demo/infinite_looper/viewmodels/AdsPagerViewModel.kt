package com.tulau.demo.infinite_looper.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tulau.demo.models.DEMO_MODEL

class AdsPagerViewModel : ViewModel() {
    var storiesData: ArrayList<DEMO_MODEL>? = null

    init {
        storiesData = arrayListOf(
            DEMO_MODEL(description = "https://go2joy.s3-ap-southeast-1.amazonaws.com/banner/3850_1635732162_617f4ac2cc552.png"),
            DEMO_MODEL(description = "https://go2joy.s3-ap-southeast-1.amazonaws.com/banner/3731_1636957054_6191fb7e6ff16.jpg"),
            DEMO_MODEL(description = "https://go2joy.s3-ap-southeast-1.amazonaws.com/banner/3850_1633081872_6156da10e7a12.png"),
            DEMO_MODEL(description = "https://go2joy.s3-ap-southeast-1.amazonaws.com/banner/4479_1636689498_618de65a000bc.jpg"),
            DEMO_MODEL(description = "https://go2joy.s3-ap-southeast-1.amazonaws.com/banner/3850_1635820788_6180a4f472560.png"),
        )
    }
}