package com.tulau.demo.infinite_looper.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.tulau.demo.R
import com.tulau.demo.custom_views.CustomViewPager
import com.tulau.demo.models.DEMO_MODEL
import com.tulau.demo.utils.GlideApp
import java.util.*

class DemoInfiniteAdapter(itemList: ArrayList<DEMO_MODEL>, isInfinite: Boolean) : CustomViewPager.CustomPagerAdapter<DEMO_MODEL>(itemList, isInfinite) {

    override fun inflateView(viewType: Int, container: ViewGroup, listPosition: Int): View = LayoutInflater.from(container.context).inflate(R.layout.item_ads, container, false)

    override fun bindView(convertView: View, listPosition: Int, viewType: Int) {
        convertView.findViewById<ImageView>(R.id.iv_ads).let {
            GlideApp.with(it.context)
                .load(itemList?.get(listPosition)?.description ?: "")
                .apply(RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(it)
        }
    }
}