package com.tulau.demo.custom_views

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.SparseArray
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager

/**
 * Created by Tư Lầu ✪ on 25/11/2021 - 8:39 PM.
 */
open class CustomViewPager : ViewPager {
    var isInfinite = true
    var isAutoScroll = false


    private var timer = 1000
    private var currentPagePosition = 0
    private var isAutoScrollResumed = false
    private val autoScrollHandler = Handler(Looper.getMainLooper())
    private val autoScrollRunnable = Runnable {
        if (adapter == null || !isAutoScroll || (adapter?.count ?: 0) < 2) return@Runnable
        if (!isInfinite && (adapter?.count ?: (0 - 1)) == currentPagePosition) {
            currentPagePosition = 0
        } else {
            currentPagePosition++
        }
        setCurrentItem(currentPagePosition, true)
    }


    private var previousScrollState = SCROLL_STATE_IDLE
    private var scrollState = SCROLL_STATE_IDLE

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    fun init() {
        addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
            }

            override fun onPageSelected(position: Int) {
                currentPagePosition = position
                if (isAutoScrollResumed) {
                    autoScrollHandler.removeCallbacks(autoScrollRunnable)
                    autoScrollHandler.postDelayed(autoScrollRunnable, timer.toLong())
                }

            }

            override fun onPageScrollStateChanged(state: Int) {
                previousScrollState = scrollState
                scrollState = state
                if (state == SCROLL_STATE_IDLE) {
                    if (isInfinite) {
                        if (adapter == null) return
                        val itemCount = adapter?.count ?: 0
                        if (itemCount < 2) {
                            return
                        }
                        val index = currentItem
                        if (index == 0) {
                            setCurrentItem(itemCount - 2, false)
                        } else if (index == itemCount - 1) {
                            setCurrentItem(1, false)
                        }
                    }
                }
            }
        })
        if (isInfinite) setCurrentItem(1, false)
    }

    override fun setAdapter(adapter: PagerAdapter?) {
        super.setAdapter(adapter)
        if (isInfinite) setCurrentItem(1, false)
    }

    fun resumeAutoScroll() {
        isAutoScrollResumed = true
        autoScrollHandler.postDelayed(autoScrollRunnable, timer.toLong())
    }

    fun pauseAutoScroll() {
        isAutoScrollResumed = false
        autoScrollHandler.removeCallbacks(autoScrollRunnable)
    }

    fun setTimer(timer: Int) {
        this.timer = timer
        resetAutoScroll()
    }

    private fun resetAutoScroll() {
        pauseAutoScroll()
        resumeAutoScroll()
    }

    private fun getRealPosition(position: Int): Int {
        if (!isInfinite || adapter == null) return position
        return if (position == 0) {
            adapter!!.count - 1 - 2 //First item is a dummy of last item
        } else if (position > adapter!!.count - 2) {
            0 //Last item is a dummy of first item
        } else {
            position - 1
        }
    }


    abstract class CustomPagerAdapter<T>(itemList: List<T>, isInfinite: Boolean) : PagerAdapter() {
        val listCount: Int
            get() = itemList?.size ?: 0

        val lastItemPosition: Int
            get() = if (isInfinite) {
                itemList?.size ?: 0
            } else {
                itemList?.size ?: 1 - 1
            }

        var itemList: List<T>? = null
            set(value) {
                field = value
                viewCache = SparseArray()
                canInfinite = (itemList?.size ?: 0) > 1
                notifyDataSetChanged()
            }
        private var viewCache = SparseArray<View>()
        var isInfinite = false
            protected set
        private var canInfinite = true
        private var dataSetChangeLock = false

        init {
            this.isInfinite = isInfinite
            this.itemList = itemList
        }


        protected abstract fun inflateView(viewType: Int, container: ViewGroup, listPosition: Int): View

        protected abstract fun bindView(convertView: View, listPosition: Int, viewType: Int)

        fun getItem(listPosition: Int): T? {
            return if (listPosition >= 0 && listPosition < (itemList?.size ?: 0)) {
                itemList?.get(listPosition)
            } else {
                null
            }
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val listPosition = if (isInfinite && canInfinite) getListPosition(position) else position
            val viewType = getItemViewType(listPosition)
            val convertView: View
            if (viewCache[viewType, null] == null) {
                convertView = inflateView(viewType, container, listPosition)
            } else {
                convertView = viewCache[viewType]
                viewCache.remove(viewType)
            }
            bindView(convertView, listPosition, viewType)
            container.addView(convertView)
            return convertView
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object`
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val listPosition =
                if (isInfinite && canInfinite) getListPosition(position) else position
            container.removeView(`object` as View)
            if (!dataSetChangeLock) viewCache.put(
                getItemViewType(listPosition),
                `object`
            )
        }

        override fun notifyDataSetChanged() {
            dataSetChangeLock = true
            super.notifyDataSetChanged()
            dataSetChangeLock = false
        }

        override fun getItemPosition(`object`: Any): Int = POSITION_NONE

        override fun getCount(): Int {
            val count = itemList?.size ?: 0
            return if (isInfinite && canInfinite) {
                count + 2
            } else {
                count
            }
        }


        protected open fun getItemViewType(listPosition: Int): Int = 0

        private fun getListPosition(position: Int): Int {
            if (!(isInfinite && canInfinite)) return position
            return when {
                position == 0 -> {
                    count - 1 - 2 //First item is a dummy of last item
                }
                position > count - 2 -> {
                    0 //Last item is a dummy of first item
                }
                else -> {
                    position - 1
                }
            }
        }

    }
}